package com.example.a300cem;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class LoginTest {
    @Rule
    public ActivityTestRule<LoginActivity> EditInformationActivity = new ActivityTestRule<>(
            LoginActivity.class);
    @Test
    public void Login() {
        onView(withId(R.id.editText)).perform(typeText("vtc170176114@gmail.com"),closeSoftKeyboard());
        onView(withId(R.id.editText2)).perform(typeText("123456"),closeSoftKeyboard());
        onView(withId(R.id.button2)).perform(click());
    }
}
