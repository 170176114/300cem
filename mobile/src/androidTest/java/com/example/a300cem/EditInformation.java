package com.example.a300cem;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class EditInformation {
    @Rule
    public ActivityTestRule<EditInformationActivity> EditInformationActivity = new ActivityTestRule<>(
            EditInformationActivity.class);
    @Test
    public void EditHeight() {
        onView(withId(R.id.edit_height)).perform(typeText("180"),closeSoftKeyboard());
        onView(withId(R.id.save)).perform(click());
        onView(withId(R.id.navigation_dashboard)).perform(click());
        onView(withId(R.id.text_height)).check(matches(withText("180 cm")));

    }
    @Test
    public void EditWeight() {
        onView(withId(R.id.edit_weight)).perform(typeText("75"),closeSoftKeyboard());
        onView(withId(R.id.save)).perform(click());
        onView(withId(R.id.navigation_dashboard)).perform(click());
        onView(withId(R.id.text_weight)).check(matches(withText("75 KG")));

    }
}
