package com.example.a300cem;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

public class ResetPasswordActivityTest {
    @Rule
    public ActivityTestRule<ResetPasswordActivity> ResetPasswordActivity = new ActivityTestRule<>(
            ResetPasswordActivity.class);
    @Test
    public void ForgetPassword() {
        onView(withId(R.id.email)).perform(typeText("vtc170176114@gmail.com"),closeSoftKeyboard());
        onView(withId(R.id.ForgetPWD)).perform(click());

    }
}