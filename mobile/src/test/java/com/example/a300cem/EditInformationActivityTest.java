package com.example.a300cem;


import org.junit.Test;

import static org.junit.Assert.*;

public class EditInformationActivityTest {

    @Test
    public  void testHeight() throws Exception{
        String height = "177";
        assertEquals(height, "177");
    }

    @Test
    public  void testWeight() throws Exception{
        String weight = "70";
        assertEquals(weight, "70");
    }
}