package com.example.a300cem.StepCount;

public interface StepListener {

    public void step(long timeNs);

}
