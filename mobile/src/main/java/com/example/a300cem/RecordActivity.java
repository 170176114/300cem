package com.example.a300cem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.a300cem.Model.Records;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RecordActivity extends AppCompatActivity {
   // ListView listView;
    DatabaseReference databaseReference;
    RecyclerView recyclerView;

//    ArrayList<String> myArrayList = new ArrayList<>();
    ArrayList<Records> list;
    MyAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        String currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser().getUid() ;
        System.out.println("123345: " + currentFirebaseUser);
        databaseReference = FirebaseDatabase.getInstance().getReference(currentFirebaseUser);


        //final ArrayAdapter<String> myArrayAdapter = new ArrayAdapter<String>(RecordActivity.this, android.R.layout.simple_list_item_1,myArrayList);

       // listView = findViewById(R.id.listview);
//        listView.setAdapter(myArrayAdapter);
    recyclerView = findViewById(R.id.myRecycler);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));

    list = new ArrayList<Records>();

    databaseReference.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


            for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                Records r = dataSnapshot1.getValue(Records.class);
                list.add(r);
            }

            adapter = new MyAdapter(RecordActivity.this, list);
            recyclerView.setAdapter(adapter);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    });






//        databaseReference.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                String value = dataSnapshot.getValue(Records.class).toString();
//
////                myArrayList.add(value);
////                myArrayAdapter.notifyDataSetChanged();
////                System.out.println("123L " + dataSnapshot.getValue());
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//               // myArrayAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });



        Button GoTo = findViewById(R.id.GoTo);

        GoTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RecordActivity.this, MapsActivity.class);
                startActivity(i);
            }
        });

    }
}
