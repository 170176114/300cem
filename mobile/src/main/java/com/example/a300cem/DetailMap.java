package com.example.a300cem;

import androidx.fragment.app.FragmentActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import com.example.a300cem.Model.Details;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

public class DetailMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    TextView step, date, km, kcal;
    String CountStep, DateAndTime, Km, Kcal;
    String Location;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        step = findViewById(R.id.Step);
        date = findViewById(R.id.Date);
        km = findViewById(R.id.Km);
        kcal = findViewById(R.id.kcal);

        getData();
        setData();
        System.out.println("kcal: " + Kcal);


       // Details[] respone = new Gson().fromJson(Location, Details[].class);



//        for (Details s : respone) {
//            System.out.println("Location Lat: " + s.getLat());
//        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        int j = 1;
        Details[] respone = new Gson().fromJson(Location, Details[].class);
        mMap.animateCamera(CameraUpdateFactory.newLatLng( new LatLng(Double.parseDouble(respone[0].getLat()), Double.parseDouble(respone[0].getLog())   )  ));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(respone[0].getLat()), Double.parseDouble(respone[0].getLog())   ) , 17));

        for (int i = 0; i < respone.length-1; i++){
         System.out.println("Location Lat: " + respone[i].getLat()+ " Log: "+ respone[i].getLog());

            mMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(Double.parseDouble(respone[i].getLat()), Double.parseDouble(respone[i].getLog())), new LatLng( Double.parseDouble(respone[j].getLat()), Double.parseDouble(respone[j].getLog()) ) ).width(15).color(Color.BLUE)
            );
            j++;
        }
//            mMap.addPolyline(new PolylineOptions()
//                    .add(new LatLng(Double.parseDouble(respone[2].getLat()), Double.parseDouble(respone[2].getLog())), new LatLng( Double.parseDouble(respone[3].getLat()), Double.parseDouble(respone[3].getLog()) ) ).width(15).color(Color.BLUE)
//            );

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void getData(){
        if(getIntent().hasExtra("data1") && getIntent().hasExtra("data2") && getIntent().hasExtra("data3") && getIntent().hasExtra("data4")  && getIntent().hasExtra("data5")  ){
            CountStep = getIntent().getStringExtra("data1");
            DateAndTime = getIntent().getStringExtra("data2");
            Km = getIntent().getStringExtra("data3");
            Location = getIntent().getStringExtra("data5");
            Kcal = getIntent().getStringExtra("data4");
            System.out.println("data1: " + Kcal);
        }else {

        }
    }
    private  void setData(){
        step.setText(CountStep);
        date.setText(DateAndTime);
        km.setText(Km);
        kcal.setText(Kcal);

    }

}
