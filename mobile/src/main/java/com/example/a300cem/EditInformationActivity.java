package com.example.a300cem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.a300cem.Model.UserInformation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class EditInformationActivity extends AppCompatActivity {
    EditText height, weight;
    ImageButton save;
    DatabaseReference databaseReference, myRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_information);

        height = (EditText) findViewById(R.id.edit_height);
        weight = (EditText) findViewById(R.id.edit_weight);
        save = findViewById(R.id.save);

        String currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser().getUid() ;
        System.out.println("123345: " + currentFirebaseUser);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("user").child(currentFirebaseUser);
        final FirebaseDatabase data = FirebaseDatabase.getInstance();
        myRef = data.getReference().child("user").child(currentFirebaseUser);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final UserInformation value = dataSnapshot.getValue(UserInformation.class);
                height.setText(value.getHeight());
                weight.setText(value.getWeight());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String H = height.getText().toString();
                String W = weight.getText().toString();

                System.out.println("weight: " + W + " height: " + H);
                Map<String, String> userMap = new HashMap<>();
                userMap.put("height", H);
                userMap.put("weight", W);
                databaseReference.setValue(userMap);

                Intent i = new Intent(EditInformationActivity.this, BottomNavigationActivity.class);
                startActivity(i);

            }
        });

    }
}
