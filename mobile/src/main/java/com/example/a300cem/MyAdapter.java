package com.example.a300cem;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300cem.Model.Records;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    Context context;
    ArrayList<Records> records;
    Object Location;


    public MyAdapter(Context c , ArrayList<Records> r){
        context = c;
        records = r;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.designlistview,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Location = records.get(position).getLocation();
        Gson gson = new Gson();
        final String all = gson.toJson(Location);


        // Object need to array


        holder.step.setText(records.get(position).getStep());
        holder.date.setText(records.get(position).getDateAndTime());
        holder.km.setText(records.get(position).getDistance());

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailMap.class);
                intent.putExtra("data1", records.get(position).getStep());
                intent.putExtra("data2", records.get(position).getDateAndTime());
                intent.putExtra("data3", records.get(position).getDistance());
                intent.putExtra("data4", all);
                context.startActivity(intent);

            }
        });

    }
    

    @Override
    public int getItemCount() {
        return records.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout mainLayout;

        TextView step, date, km;
        public MyViewHolder(View itemView){
            super(itemView);
            step =  itemView.findViewById(R.id.listStep);
            date = itemView.findViewById(R.id.Date);
            km = itemView.findViewById(R.id.Km);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }
}
