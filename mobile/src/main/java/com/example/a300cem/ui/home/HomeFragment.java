package com.example.a300cem.ui.home;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a300cem.DetailMap;
import com.example.a300cem.MapsActivity;
import com.example.a300cem.Model.Records;
import com.example.a300cem.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.gson.Gson;

import java.util.ArrayList;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class HomeFragment extends Fragment{
    private RecyclerView recyclerView;
    DatabaseReference databaseReference;
    ArrayList<Records> list;
    private FirebaseRecyclerAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View RecordsView = inflater.inflate(R.layout.fragment_home, container, false);
        String currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        System.out.println("123345: " + currentFirebaseUser);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("item").child(currentFirebaseUser);
        recyclerView = (RecyclerView) RecordsView.findViewById(R.id.myRecycler);

        Button GoTo = (Button) RecordsView.findViewById(R.id.GoTo);

        GoTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MapsActivity.class);
                startActivity(i);
            }
        });


        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

//        list = new ArrayList<Records>();
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                list.clear();
//                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
//                    Records r = dataSnapshot1.getValue(Records.class);
//                    list.add(r);
//                }
//                Adapter adapter = new Adapter(list);
//                recyclerView.setAdapter(adapter);
//
////                for (int i = 0 ; i< list.size(); i++){
////                    System.out.println("data test: " + list.get(0).getDateAndTime());
////                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        Query query = databaseReference;
        FirebaseRecyclerOptions<Records> options = new FirebaseRecyclerOptions.Builder<Records>().setQuery(query, new SnapshotParser<Records>() {
            @NonNull
            @Override
            public Records parseSnapshot(@NonNull DataSnapshot snapshot) {
                return new Records(snapshot.child("Step").getValue().toString(),
                        snapshot.child("DateAndTime").getValue().toString(),
                        snapshot.child("Distance").getValue().toString(),
                        snapshot.child("Location").getValue().toString(),
                        snapshot.child("Kcal").getValue().toString());
            }
        }).build();

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                adapter.getRef(position).removeValue();
            }

            @Override
            public void onChildDraw (Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive){
                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addBackgroundColor(ContextCompat.getColor(getActivity(), R.color.Red))
                        .addActionIcon(R.drawable.ic_delete_white_24dp)
                        .create()
                        .decorate();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        }).attachToRecyclerView(recyclerView);



        adapter = new FirebaseRecyclerAdapter<Records, ViewHolder>(options) {
            private ArrayList<Records> records;
            Context context;
            Object Location;

            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int i, @NonNull final Records records) {
                Location = records.getLocation();
                Gson gson = new Gson();
                final String all = gson.toJson(Location);
                // Object need to array


                holder.step.setText(records.getStep());
                holder.date.setText(records.getDateAndTime());
                holder.km.setText(records.getDistance());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), DetailMap.class);
                        intent.putExtra("data1", records.getStep());
                        intent.putExtra("data2", records.getDateAndTime());
                        intent.putExtra("data3", records.getDistance());
                        intent.putExtra("data4", all);
                        intent.putExtra("data5", records.getKcal());
                        startActivity(intent);
                    }
                });
            }



            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.designlistview, parent, false);

                return new ViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
        adapter.startListening();




        return RecordsView;
    }

//    public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
//        private ArrayList<Records> records;
//        Context context;
//        Object Location;
//        String Kcal;
//
//        public Adapter(ArrayList<Records> list) {
//            this.records = list;
//        }
//
//
//        @NonNull
//        @Override
//        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.designlistview, parent, false);
//
//            ViewHolder viewHolder = new ViewHolder(view);
//            return viewHolder;
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
//            Location = records.get(position).getLocation();
//            Gson gson = new Gson();
//            final String all = gson.toJson(Location);
//            Kcal = records.get(position).getKcal();
//            // Object need to array
//
//
//            holder.step.setText(records.get(position).getStep());
//            holder.date.setText(records.get(position).getDateAndTime());
//            holder.km.setText(records.get(position).getDistance());
//
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(getContext(), DetailMap.class);
//                    intent.putExtra("data1", records.get(position).getStep());
//                    intent.putExtra("data2", records.get(position).getDateAndTime());
//                    intent.putExtra("data3", records.get(position).getDistance());
//                    intent.putExtra("data4", all);
//                    intent.putExtra("data5", Kcal);
//                    startActivity(intent);
//                }
//            });
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return records.size();
//        }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mainLayout;

        TextView step, date, km;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            step = itemView.findViewById(R.id.listStep);
            date = itemView.findViewById(R.id.Date);
            km = itemView.findViewById(R.id.Km);
            mainLayout = itemView.findViewById(R.id.mainLayout);
        }
    }

}




