package com.example.a300cem.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.a300cem.EditInformationActivity;
import com.example.a300cem.LoginActivity;
import com.example.a300cem.Model.UserInformation;
import com.example.a300cem.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DashboardFragment extends Fragment {
    DatabaseReference myRef;

    ImageButton btn_logout, btn_edit;
    TextView weight, height;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        btn_edit = root.findViewById(R.id.edit);
        btn_logout = root.findViewById(R.id.logout);
        weight = root.findViewById(R.id.text_weight);
        height = root.findViewById(R.id.text_height);


        String currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser().getUid() ;
        final FirebaseDatabase data = FirebaseDatabase.getInstance();
        myRef = data.getReference().child("user").child(currentFirebaseUser);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final UserInformation value = dataSnapshot.getValue(UserInformation.class);
                weight.setText(value.getWeight() + " KG");
                height.setText(value.getHeight() + " cm");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditInformationActivity.class);
                startActivity(i);
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intToMain = new Intent(getActivity(), LoginActivity.class);
                startActivity(intToMain);
            }
        });


        return root;
    }
}
