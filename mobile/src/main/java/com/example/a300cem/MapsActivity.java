package com.example.a300cem;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a300cem.Model.Records;
import com.example.a300cem.Model.UserInformation;
import com.example.a300cem.StepCount.StepDetector;
import com.example.a300cem.StepCount.StepListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, SensorEventListener, StepListener {

    private GoogleMap mMap;
    Location currentLocation;
    double kcal;
    Button start;
    Button stop;

    protected LocationManager locationManager;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;
    ArrayList<Double> Latitude = new ArrayList<Double>();
    ArrayList<Double> Longitude = new ArrayList<Double>();
    ArrayList<Double> Distance = new ArrayList<Double>();
    Double AllDistance = 0.0;

    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private static final String TEXT_NUM_STEPS = " ";
    private int numSteps;

    DatabaseReference Database, myRef;

    Double latittude;
    Double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);



        //fetchLastLocation();


        start = findViewById(R.id.btn_start);
        stop = findViewById(R.id.btn_stop);
        stop.setVisibility(View.GONE);



        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);




        String currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser().getUid() ;
        Database = FirebaseDatabase.getInstance().getReference().child("item").child(currentFirebaseUser);
        final FirebaseDatabase data = FirebaseDatabase.getInstance();
        myRef = data.getReference().child("user").child(currentFirebaseUser);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            final UserInformation value = dataSnapshot.getValue(UserInformation.class);

                stop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (int i = 0; i < Latitude.size()-1; i++){
                            Distance.add(distance(Latitude.get(i),Latitude.get(i+1), Longitude.get(i),Longitude.get(i+1)));
                        }
                        for (int i = 0; i < Distance.size(); i++){
                            AllDistance = AllDistance + Distance.get(i);
                        }
                        Double KM = Math.round(AllDistance * 100.0)/100.0;


                        kcal = Double.parseDouble(value.getWeight()) * KM * 1.036;

                        sensorManager.unregisterListener(MapsActivity.this);
                        String Step = Integer.toString(numSteps);
                        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
                        System.out.println(timeStamp);
                        String id = Database.push().getKey();
                        Map<String, String> userMap = new HashMap<>();
//                userMap.put("UserID", UserID);
                        userMap.put("Step", Step);
                        userMap.put("DateAndTime", timeStamp);
                        userMap.put("Distance", KM.toString() );
                       userMap.put("Kcal", String.valueOf(kcal));
                        Database.child(id).setValue(userMap);

                        Map<String, String> location = new HashMap<>();
                        for (int i =0; i < Latitude.size(); i++){
                            // String key = Database.push().getKey();
                            location.put("Lat", Latitude.get(i).toString() );
                            location.put("Log", Longitude.get(i).toString() );
                            Database.child(id).child("Location").child(String.valueOf(i)).setValue(location);
                        }






                        Intent i = new Intent(MapsActivity.this, BottomNavigationActivity.class);
                        startActivity(i);
                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numSteps = 0;
                sensorManager.registerListener(MapsActivity.this, accel, SensorManager.SENSOR_DELAY_FASTEST);
                start.setVisibility(View.GONE);
                stop.setVisibility(View.VISIBLE);

            }
        });






    }
    public static double distance(double lat1, double lat2, double lon1, double lon2) {

        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));

        // Radius of earth in kilometers. Use 3956
        // for miles
        double r = 6371;

        // calculate the result
        return(c * r);
    }

    @Override
    protected void onStart(){
        super.onStart();
        fetchLastLocation();
    }


    private void fetchLastLocation() {

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[] {ACCESS_FINE_LOCATION}, REQUEST_CODE );
            return;
        }



        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
            if (location != null){
                currentLocation = location;
                System.out.println("LAT :" + currentLocation.getLatitude() + " LONG : " + currentLocation.getLongitude());
                Latitude.add(currentLocation.getLatitude());
                Longitude.add(currentLocation.getLongitude());
//                for (int i = 0; i < Latitude.size(); i++) {
//                    System.out.println("123" + "  " +Latitude.get(i) + " " + Longitude.get(i));
//                }

                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(MapsActivity.this);
                }
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("Start");
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
        //googleMap.addMarker(markerOptions);

        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>(){
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    latittude = location.getLatitude();
                    longitude = location.getLongitude();

                    mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(latittude, longitude), new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                .width(15)
                .color(Color.RED));
                }
            }
        });

//        googleMap.addPolyline(new PolylineOptions()
//                .add(new LatLng(StartLat, StartLong), new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
//                .width(30)
//                .color(Color.RED));

    }
   @Override
   public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
//                    fetchLastLocation();
                }
                break;
        }

   }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;

        Latitude.add(currentLocation.getLatitude());
        Longitude.add(currentLocation.getLongitude());
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(MapsActivity.this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }
    @Override
    public void step(long timeNs) {
        TextView TvSteps = findViewById(R.id.tv_steps);
        numSteps++;
        TvSteps.setText(TEXT_NUM_STEPS + numSteps);
    }




}
