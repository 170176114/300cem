package com.example.a300cem.Model;

public class Details {
    String Lat, Log;

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLog() {
        return Log;
    }

    public void setLog(String log) {
        Log = log;
    }

    public Details(String lat, String log) {
        Lat = lat;
        Log = log;
    }
}
