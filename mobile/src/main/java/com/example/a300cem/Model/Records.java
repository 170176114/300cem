package com.example.a300cem.Model;


public class Records {
    private String Step;
    private String DateAndTime;
    private String Distance;
    private String Kcal;
    private Object Location;


    public String getStep() {
        return Step;
    }

    public String getDateAndTime() {
        return DateAndTime;
    }

    public String getDistance() {
        return Distance;
    }

    public Records() {
    }

    public Object getLocation() {
        return Location;
    }


    public void setStep(String step) {
        Step = step;
    }

    public void setDateAndTime(String dateAndTime) {
        DateAndTime = dateAndTime;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public void setKcal(String kcal) {
        Kcal = kcal;
    }

    public void setLocation(Object location) {
        Location = location;
    }

    public String getKcal() {
        return Kcal;
    }

    public Records(String step, String dateAndTime, String distance, String kcal, Object location) {
        Step = step;
        DateAndTime = dateAndTime;
        Distance = distance;
        Kcal = kcal;
        Location = location;
    }
}
