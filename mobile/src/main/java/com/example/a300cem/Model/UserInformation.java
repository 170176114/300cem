package com.example.a300cem.Model;

public class UserInformation {
    String height;
    String weight;

    public UserInformation() {
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight() {
        return weight;
    }

    public UserInformation(String height, String weight) {
        this.height = height;
        this.weight = weight;
    }
}
